import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class mainFrame extends JFrame {

	private JPanel contentPane;
	private JPanel functionPanel;
	private JPanel orderPanel;
	private JPanel totalPanel;
	private JPanel customerPanel;
	private JPanel foodChoicePanel;
	private JPanel foodVariatyPanel;
	private JPanel sideBarPanel;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainFrame frame = new mainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public mainFrame() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, screenSize.width, screenSize.height);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setSize(screenSize);
		contentPane.setLayout(null);
		
		functionPanel = new JPanel();
		functionPanel.setBounds(0, 0, 1902, 87);
		contentPane.add(functionPanel);
		
		orderPanel = new JPanel();
		orderPanel.setBounds(0, 87, 427, 469);
		contentPane.add(orderPanel);
		
		totalPanel = new JPanel();
		totalPanel.setBounds(0, 555, 427, 80);
		contentPane.add(totalPanel);
		
		customerPanel = InputPanel.getInputPanel(0, 633, 427, 400);
		contentPane.add(customerPanel);
		
		foodChoicePanel = new JPanel();
		foodChoicePanel.setBounds(426, 87, 1395, 548);
		contentPane.add(foodChoicePanel);
		
		foodVariatyPanel = new JPanel();
		foodVariatyPanel.setBounds(426, 633, 1395, 400);
		contentPane.add(foodVariatyPanel);
		
		sideBarPanel = new JPanel();
		sideBarPanel.setBounds(1820, 87, 82, 946);
		contentPane.add(sideBarPanel);
	}
}
