import javax.swing.JPanel;
import java.awt.EventQueue;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.SystemColor;
import java.awt.Toolkit;

public class InputPanel extends JPanel {
	
	private static InputPanel inputPanel;
	
	private static int x;
	private static int y;
	private static int width;
	private static int height;
	private static int padding = 2;
	private static int space =1;

	private JLabel nameLabel;
	private JLabel phoneLabel;
	private JLabel streetLabel;
	private JLabel zipCodeLabel;
	private JLabel noteLabel;
	
	private JTextField nameInput;
	private JTextField phoneInput;
	private JTextField streetInput;
	private JTextField zipCodeInput;
	private JTextField noteInput;
	
	private JRadioButton carryOutRButton;
	private JRadioButton deliveryRButton;
	private JRadioButton walkedInRButton;
	private ButtonGroup orderOption = new ButtonGroup();
	
	private String[] customerInfo = new String[6];
	
	private JButton nextButton;

	/**
	 * Create the panel.
	 */
	private InputPanel() {

	}
	
	public static InputPanel getInputPanel(int startX, int startY, int panelWidth, int panelHeight) {
		if(inputPanel==null) {
			inputPanel = new InputPanel();
			x = startX;
			y = startY;
			width = panelWidth;
			height = panelHeight;
			inputPanel.initialize();
		}
		return inputPanel;
	}
	public String[] getCustomerInfo(){
		return customerInfo.clone();
	}
	private void initialize() {
		System.out.println("called initialize");
		int labelLength = (width-4)/3;
		int labelHeight = (height-14)/7;
		int textfieldLength = labelLength*2;
		int textfieldHeight = labelHeight;
		int cx = x+padding;
		int cy = y+padding;
		
		inputPanel.setFont(new Font("Arial", Font.PLAIN, 13));
		inputPanel.setBackground(Color.LIGHT_GRAY);
		inputPanel.setBounds(x, y, width, height);
		inputPanel.setLayout(null);
		
		nameLabel = new JLabel("Customer Name:");
		nameLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		nameLabel.setBounds(cx,cy,labelLength,labelHeight);
		inputPanel.add(nameLabel);
		
		nameInput = new JTextField(textfieldLength);
		nameInput.setFont(new Font("Arial", Font.PLAIN, 13));
		nameInput.setBounds(cx+labelLength, cy, textfieldLength, textfieldHeight);
		inputPanel.add(nameInput);
		
		cy=cy+space+labelHeight;
		phoneLabel = new JLabel("Phone: ");
		phoneLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		phoneLabel.setBounds(cx, cy, labelLength, labelHeight);
		inputPanel.add(phoneLabel);
		
		phoneInput = new JTextField(textfieldLength);
		phoneInput.setFont(new Font("Arial", Font.PLAIN, 13));
		phoneInput.setBounds(cx+labelLength, cy, textfieldLength, textfieldHeight);
		inputPanel.add(phoneInput);
		
		cy=cy+space+labelHeight;
		streetLabel = new JLabel("Street:");
		streetLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		streetLabel.setBounds(cx, cy, labelLength, labelHeight);
		inputPanel.add(streetLabel);
		
		streetInput = new JTextField(textfieldLength);
		streetInput.setFont(new Font("Arial", Font.PLAIN, 13));
		streetInput.setBounds(cx+labelLength, cy, textfieldLength, textfieldHeight);
		inputPanel.add(streetInput);
		
		cy=cy+space+labelHeight;
		zipCodeLabel = new JLabel("Zip Code: ");
		zipCodeLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		zipCodeLabel.setBounds(cx, cy, labelLength, labelHeight);
		inputPanel.add(zipCodeLabel);
		
		zipCodeInput = new JTextField(textfieldLength);
		zipCodeInput.setFont(new Font("Arial", Font.PLAIN, 13));
		zipCodeInput.setBounds(cx+labelLength, cy, textfieldLength, textfieldHeight);
		inputPanel.add(zipCodeInput);
		
		cy=cy+space+labelHeight;
		noteLabel = new JLabel("Delivery Note: ");
		noteLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		noteLabel.setBounds(cx, cy, labelLength, labelHeight);
		inputPanel.add(noteLabel);
		
		noteInput = new JTextField(textfieldLength);
		noteInput.setFont(new Font("Arial", Font.PLAIN, 13));
		noteInput.setBounds(cx+labelLength, cy, textfieldLength, textfieldHeight*2+1);
		inputPanel.add(noteInput);
		System.out.println("added note textfield with x,y: "+cx+","+cy);
		
		cy=cy+space+labelHeight;
		int buttonLength = (width-6)/3;
		int buttonHeight = labelHeight;
		carryOutRButton = new JRadioButton("Carry Out");
		carryOutRButton.setFont(new Font("Arial", Font.PLAIN, 13));
		carryOutRButton.setBounds(cx, cy, buttonLength, buttonHeight);
		inputPanel.add(carryOutRButton);
		System.out.println("added carrayOutbutton with x,y: "+cx+","+cy);
		
		cy=cy+space+buttonHeight;
		deliveryRButton = new JRadioButton("Delivery");
		deliveryRButton.setFont(new Font("Arial", Font.PLAIN, 13));
		deliveryRButton.setBounds(cx,cy,buttonLength,buttonHeight);
		inputPanel.add(deliveryRButton);
		
		walkedInRButton = new JRadioButton("Walk in");
		walkedInRButton.setFont(new Font("Arial", Font.PLAIN, 13));
		walkedInRButton.setBounds(cx+space+buttonLength,cy,buttonLength,buttonHeight);
		inputPanel.add(walkedInRButton);
		
		orderOption.add(carryOutRButton);
		orderOption.add(deliveryRButton);
		orderOption.add(walkedInRButton);
		
		nextButton = new JButton("Next");
		nextButton.setFont(new Font("Arial", Font.PLAIN, 13));
		nextButton.setBackground(Color.GREEN);
		nextButton.setForeground(Color.BLACK);
		nextButton.setBounds(cx+space*2+buttonLength*2,cy,buttonLength,buttonHeight);
		inputPanel.add(nextButton);
		
		nextButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkInfo()==true) {
					if(carryOutRButton.isSelected())
						updateCustomerInfo(nameInput.getText(),phoneInput.getText(),streetInput.getText(),
								zipCodeInput.getText(),noteInput.getText(),"carry out");
					else
						if(walkedInRButton.isSelected())
							updateCustomerInfo(nameInput.getText(),phoneInput.getText(),streetInput.getText(),
									zipCodeInput.getText(),noteInput.getText(),"walked in");
						else
							updateCustomerInfo(nameInput.getText(),phoneInput.getText(),streetInput.getText(),
									zipCodeInput.getText(),noteInput.getText(),"delivery");
				}
			}
		});
	}
	private void updateCustomerInfo(String name, String phone, String address, String zipCode, String note, String orderType) {
		customerInfo = new String[]{name,phone,address,zipCode,note,orderType};
	}
	private boolean checkInfo() {
		if(carryOutRButton.isSelected()||walkedInRButton.isSelected()||deliveryRButton.isSelected()) {
			//one of the order options(carry out, delivery, walked in) must be selected.
			if(!(nameInput.getText().isEmpty())) {       // customer name is required for every order
				System.out.println("name input is not null:"+nameInput.getText());
				if(carryOutRButton.isSelected()) {
					System.out.println("carry out button clicked");
					// only customer name and phone number is required for every carry out orders.
					if(!(phoneInput.getText().isEmpty()))
						return true;
					return false;
				}
				if(deliveryRButton.isSelected()) {
					System.out.println("delivery button clicked");
					// customer name, phone number, address is all required for delivery orders.
					if((!zipCodeInput.getText().isEmpty())&&(!streetInput.getText().isEmpty())&&(!zipCodeInput.getText().isEmpty()))
						return true;
					return false;
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
					JFrame window = new JFrame();
					window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					window.getContentPane().setLayout(null);
					window.setVisible(true);
					window.setSize(screenSize);
					
					JPanel temp = InputPanel.getInputPanel(0,0,500,500);
					window.add(temp);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
