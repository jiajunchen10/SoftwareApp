import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.SystemColor;

public class Frame1 {

	private JFrame frame;
	
	JLabel nameLabel;
	JLabel phoneLabel;
	JLabel streetLabel;
	JLabel zipCodeLabel;
	JLabel noteLabel;
	
	JTextField nameInput;
	JTextField phoneInput;
	JTextField streetInput;
	JTextField zipCodeInput;
	JTextField noteInput;
	
	private JRadioButton carryOutRButton;
	private JRadioButton deliveryRButton;
	private JRadioButton walkedInRButton;
	private ButtonGroup orderOption = new ButtonGroup();
	
	private JButton nextButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame1 window = new Frame1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Frame1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Arial", Font.PLAIN, 13));
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 360, 310);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		nameInput = new JTextField();
		nameInput.setFont(new Font("Arial", Font.PLAIN, 13));
		nameInput.setBounds(128, 28, 202, 22);
		frame.getContentPane().add(nameInput);
		
		phoneInput = new JTextField();
		phoneInput.setFont(new Font("Arial", Font.PLAIN, 13));
		phoneInput.setBounds(128, 63, 202, 22);
		frame.getContentPane().add(phoneInput);
		
		streetInput = new JTextField();
		streetInput.setFont(new Font("Arial", Font.PLAIN, 13));
		streetInput.setBounds(128, 98, 202, 22);
		frame.getContentPane().add(streetInput);
		
		zipCodeInput = new JTextField();
		zipCodeInput.setFont(new Font("Arial", Font.PLAIN, 13));
		zipCodeInput.setBounds(128, 134, 202, 22);
		frame.getContentPane().add(zipCodeInput);
		
		carryOutRButton = new JRadioButton("Carry Out");
		carryOutRButton.setFont(new Font("Arial", Font.PLAIN, 13));
		carryOutRButton.setBounds(12, 229, 85, 25);
		frame.getContentPane().add(carryOutRButton);
		
		deliveryRButton = new JRadioButton("Delivery");
		deliveryRButton.setFont(new Font("Arial", Font.PLAIN, 13));
		deliveryRButton.setBounds(101, 229, 82, 25);
		frame.getContentPane().add(deliveryRButton);
		
		walkedInRButton = new JRadioButton("Walk in");
		walkedInRButton.setFont(new Font("Arial", Font.PLAIN, 13));
		walkedInRButton.setBounds(12, 198, 85, 25);
		frame.getContentPane().add(walkedInRButton);
		
		orderOption.add(carryOutRButton);
		orderOption.add(deliveryRButton);
		orderOption.add(walkedInRButton);
		
		nextButton = new JButton("Next");
		nextButton.setFont(new Font("Arial", Font.PLAIN, 13));
		nextButton.setBackground(Color.GREEN);
		nextButton.setForeground(Color.BLACK);
		nextButton.setBounds(213, 225, 117, 32);
		frame.getContentPane().add(nextButton);
		
		noteInput = new JTextField();
		noteInput.setFont(new Font("Arial", Font.PLAIN, 13));
		noteInput.setBounds(128, 168, 202, 52);
		frame.getContentPane().add(noteInput);
		
		nameLabel = new JLabel("Customer Name:");
		nameLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		nameLabel.setBounds(12, 29, 104, 17);
		frame.getContentPane().add(nameLabel);
		
		phoneLabel = new JLabel("Phone: ");
		phoneLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		phoneLabel.setBounds(67, 64, 49, 16);
		frame.getContentPane().add(phoneLabel);
		
		streetLabel = new JLabel("Street:");
		streetLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		streetLabel.setBounds(67, 99, 49, 16);
		frame.getContentPane().add(streetLabel);
		
		zipCodeLabel = new JLabel("Zip Code: ");
		zipCodeLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		zipCodeLabel.setBounds(51, 135, 65, 16);
		frame.getContentPane().add(zipCodeLabel);
		
		noteLabel = new JLabel("Delivery Note: ");
		noteLabel.setFont(new Font("Arial", Font.PLAIN, 13));
		noteLabel.setBounds(26, 169, 90, 16);
		frame.getContentPane().add(noteLabel);
		nextButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkInfo()==true)
					System.out.println("Done!");
				else
					System.out.println("please enter the correct information!");
			}
		});
	}
	private boolean checkInfo() {
		if(carryOutRButton.isSelected()||walkedInRButton.isSelected()||deliveryRButton.isSelected()) {
			//one of the order options(carry out, delivery, walked in) must be selected.
			if(!(nameInput.getText().isEmpty())) {       // customer name is required for every order
				System.out.println("name input is not null:"+nameInput.getText());
				if(carryOutRButton.isSelected()) {
					System.out.println("carry out button clicked");
					// only customer name and phone number is required for every carry out orders.
					if(!(phoneInput.getText().isEmpty()))
						return true;
					return false;
				}
				if(deliveryRButton.isSelected()) {
					System.out.println("delivery button clicked");
					// customer name, phone number, address is all required for delivery orders.
					if((!zipCodeInput.getText().isEmpty())&&(!streetInput.getText().isEmpty())&&(!zipCodeInput.getText().isEmpty()))
						return true;
					return false;
				}
				return true;
			}
		}
		return false;
	}
}
